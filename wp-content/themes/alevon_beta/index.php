		
<?php get_header(); ?>
<?php 
	include locate_template('components/stickyheader.php');
 ?>
 
 <!-- 100vw/auto container for entire site -->
<div id="siteWrapper">
	<?php 
		$bgimages = [
			get_template_directory_uri() . '/library/img/bg1.jpg',
			get_template_directory_uri() . '/library/img/bg2.jpg'
		];
		foreach ($bgimages as $bgimage) :
	 ?>
			<section class="section">
				<div class="section-background" style="background-image: url(<?php echo $bgimage; ?>)"></div>			
			</section>
	<?php endforeach; ?>
	<footer class="footer"></footer>


	<!-- container for content inside 100vw/auto container-->
	<div id="contentWrapper">
		<!-- Dummy Section for Offset (above the fold) -->
		<section class="content"></section>
		<!-- below the fold section -->
		<section class="content"><?php include locate_template('components/getposts.php'); ?></section>
		<ul class="footer-siteMeta">
			<li>Copyright &copy2017 AlessandraVonEikh.com All Rights Reserved</li>
		</ul>
	</div>
	<!-- End SiteWrapper -->
</div>

<?php include locate_template('components/contactform.php'); ?>
<?php get_footer(); ?>
