var APP = {
};
;(function ( $, APP, window, document, undefined ) {
	$(document).ready(function(){




		/*
			Parallax Scrolling Background Images with CSS translate3d
		*/
		APP.ParallaxBG = {

			images: $('.section-background'),
			wrappers: $('.section'),		// IT WAS THIS

			_init: function(){
				if(APP.ParallaxBG.images.length > 0) {
					$(window).on('scroll load', APP.ParallaxBG._scrollLoadHandler);
				}
			},
			_scrollLoadHandler: function(){
				for ( var i = 0; i < APP.ParallaxBG.images.length; i++) {
					if (i == 0) {
						$(APP.ParallaxBG.images[i]).css('transform', 'translate3d(0%, ' + APP.ParallaxBG._scaleStrength(APP.ParallaxBG.wrappers[i], 20) + ',0)');
					} else {
						$(APP.ParallaxBG.images[i]).css('transform', 'rotate(7deg) translate3d(0%, ' + APP.ParallaxBG._scaleStrength(APP.ParallaxBG.wrappers[i], 20) + ',0)');
					}
				}
			},
			_scaleStrength: function(el, strength){
				var winHeight = $(window).height();
				var winCenter = $(window).scrollTop() + ( $(window).height() / 2 );
				var winCenter = winCenter.toFixed(2);
				var divCenter = $(el).offset().top + ($(el).height() / 2);
				var divCenter = divCenter.toFixed(2);
				var midPoint = winCenter - divCenter;
				var midPoint = midPoint.toFixed(2);
				var scale = APP.ParallaxBG._map(midPoint, 0, winHeight, 0, strength);
				return 0 + scale + '%';
			},
			_map : function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0;},
		}
		APP.ParallaxBG._init();

		/*
			A beautiful fadein/out modal contact form
		*/
		APP.ContactForm = {			
			button: $('.section-main-contactme'),
			panel: $('.contactform'),
			exit: $('.contactform-close'),
			_init:function(){

				APP.ContactForm.button.on('click', APP.ContactForm._openModal);
				APP.ContactForm.exit.on('click', APP.ContactForm._closeModal);
				
				// esc key
				$(document).keyup(function(e) {
				     if (e.which == 27) {
				     	APP.ContactForm._closeModal();
				    }
				});
				// click outside the cf while the panel is open
				APP.ContactForm.panel.on('click', APP.ContactForm._navAway);

			},
			_navAway: function(e){
				if (!$(e.target).is('.contactform--reveal') ){
					e.stopPropagation();
				} else {
					APP.ContactForm._closeModal();
				}
			},
			_closeModal: function(){
				var panel = APP.ContactForm.panel;
				panel.removeClass('contactform--fade');
				panel.one('transitionend',function(e){
					panel.removeClass('contactform--reveal');
				});
			},
			_openModal: function(){
				var panel = APP.ContactForm.panel;
				panel.addClass('contactform--reveal');
				setTimeout(function(){
					panel.addClass('contactform--fade');
				}, 20);
			},
		}
		APP.ContactForm._init();




		APP.ContactSubmission = {
			reqfields: $('.wpcf7-validates-as-required'),
			reqmarker: '<i class="fa fa-exclamation cf7-required-icon"></i>',
			submitbtn: $('.wpcf7-submit'),
			_eventHandler: function(e){
				if (e.type == 'wpcf7invalid') {
					APP.ContactSubmission.reqfields.after(APP.ContactSubmission.reqmarker);
				}
				if (e.type == 'wpcf7mailsent') {
					APP.ContactSubmission.submitbtn.attr('value', 'Thanks!');
				}
			},
			_init: function(){
				document.addEventListener('wpcf7invalid', APP.ContactSubmission._eventHandler, false );
				document.addEventListener('wpcf7mailsent', APP.ContactSubmission._eventHandler, false );
			},
		}
		APP.ContactSubmission._init();





















		// this is a global component used to determine which breakpoint we're at
		// to listen for the event do $(document).on('breakpoint', eventHandlerCallback);
		// callback accepts normal amount of params that a js event listener callback would have
		// but there's a custom property added to the event object called "name"
		// assuming in the callback the event object variable passed in is e: console.log(e.device);
		APP.Breakpoint = {
			name : '',
			_init : function(){
				$(window).on('resize load', APP.Breakpoint._resizeLoadHander);
			},
			_resizeLoadHander : function(){
				if( $(window).width() > 1024 && APP.Breakpoint.name != 'desktop' ){
					APP.Breakpoint.name = 'desktop';
					APP.Breakpoint._dispatchEvent();
				}
				else if( $(window).width() <= 1024 && $(window).width() > 640 && APP.Breakpoint.name != 'tablet' ){
					APP.Breakpoint.name = 'tablet';	
					APP.Breakpoint._dispatchEvent();
				}
				else if( $(window).width() < 641 && APP.Breakpoint.name != 'mobile' ){		
					APP.Breakpoint.name = 'mobile';		
					APP.Breakpoint._dispatchEvent();
				}
			},
			_dispatchEvent : function(){
				$(document).trigger($.Event('breakpoint', {device: APP.Breakpoint.name}));
			}
		}
		APP.Breakpoint._init();		

	});

})( jQuery, APP, window, document );

