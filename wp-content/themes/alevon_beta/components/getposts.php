<?php 
	$args = array(
		'posts_per_page' => 1,
		'post_type' => 'post',
	);
	$posts = get_posts($args);
	if (!empty($posts)) :
		foreach($posts as $post) :
			setup_postdata( $post );
?>	
		<div class="section-main--wrapper">
			<div class="section-main">
				<h1 class="section-main-title"><?php the_title(); ?></h1>	
				<div class="section-main-content"><?php the_content(); ?></div>
				<div class="section-main-contactme"><p>Contact Me</p></div>
			</div>
		</div>
<?php 
		endforeach;
	endif;
?>